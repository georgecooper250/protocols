---
title: "Basic Attacker"
date: 2020-02-17
weight: 30
---
{{< pg_start >}}


In general, the network can be controlled by an adversary, who can perform the following operations:
<ul>
 <li> Intercept any message (lack of confidentiality)
 <li> Block any message (lack of availability)
 <li> Add some message pretending to be another agent (lack of integrity/authentication)
</ul>
