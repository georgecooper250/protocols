 ---
title: "Protocol Game"
date: 2020-02-16
weight: 20
---

In this section, we introduce an interactive to study security protocols. Each page contains a specific protocol, introducing a specific concept. You are then asked to try to attack this protocol. Some tips are provided, as well as a possible solution. Note that in many cases, multiple solutions are possible!

