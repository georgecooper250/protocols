
# About 


This site contains the lecture material for teaching security protocols, designed and taught by [Dr Charles Morisset](http://www.morisset.eu) at [Newcastle University](https://www.ncl.ac.uk/computing/).

This is an experimental version, aiming at replacing the Powerpoint slide deck used until now, to enable students to use the interactive tool [Protocol Game](https://gitlab.com/morisset/protocolgame/), developed by Charles Morisset.


